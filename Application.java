import java.util.Scanner;

public class Application {
	public static void main(String[] arg){
		Scanner reader = new Scanner(System.in);
		int numOfStudents = 4;
		//Intitalizing studentOne and defining the associated fields
		Student studentOne = new Student(86.2,1);
		//Intitalizing studentTwo and defining the associated fields
		Student studentTwo = new Student(58.7,2);
		//initalizing an array and adding and creating objects
		Student[] section4 = new Student[4];
		section4[0] = studentOne;
		section4[1] = studentTwo;
		section4[2] = new Student(71.3,2);
		section4[3] = new Student(65.0,3);
		section4[3].setProgram("Nursing");
		
		System.out.println(section4[1].getGrade());
		//Calls instance methods in student class
		studentTwo.greeting();
		studentTwo.movingUp();
		//Prints to terminal
		System.out.println("How much did the last student study?");
		int amountStudied = Integer.parseInt(reader.nextLine());

		System.out.println("Student 1 Study Lvl: " + section4[0].getAmountLearnt());
		System.out.println("Student 2 Study Lvl: " + section4[1].getAmountLearnt());
		System.out.println("Student 3 Study Lvl: " + section4[2].getAmountLearnt());
		section4[2].study(amountStudied);
		System.out.println("Student 3 Study Lvl: " + section4[2].getAmountLearnt());
		System.out.println("Student 4's Grades: " + section4[3].getGrade());
		System.out.println("Does Student 4 have Evening Classes?: " + section4[3].getHasEveningClasses());
		System.out.println("Student 4's Current Year : " + section4[3].getCurrentYear());
		System.out.println("Student 4's Program: " + section4[3].getProgram());

	}
}

