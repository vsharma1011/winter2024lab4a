public class Student {
	//Fields for the student
	private double grade;
	private int currentYear;
	private String program;
	private boolean hasEveningClasses;
	private int amountLearnt;
	
	public Student(double grade, int currentYear){
		this.grade = grade;
		this.currentYear = currentYear;
		this.program = "Social Science";
		this.hasEveningClasses = false;
		this.amountLearnt = 0;
	}
	
	public void movingUp(){
		if(this.grade >= 60 ){
			currentYear++;
			System.out.println("I will move on to year " + currentYear);
		}
		else{
			System.out.println("I need to redo some classes...");
		}
		
	}
	public void greeting(){
		System.out.println("I am a student who studies " + program);
		if(hasEveningClasses){
			System.out.println("I am also taking evening classes");
		}
	}
	public void study(int amountStudied){
		amountLearnt += amountStudied;
	}
	//All Set Methods
	public void setProgram(String program){
		this.program = program;
	}
	public void setHasEveningClasses(boolean hasEveningClasses){
		this.hasEveningClasses = hasEveningClasses;
	}
	//All get methods
	public double getGrade(){
		return this.grade;
	}
	public int getCurrentYear(){
		return this.currentYear;
	}
	public String getProgram(){
		return this.program;
	}
	public boolean getHasEveningClasses(){
		return this.hasEveningClasses;
	}
	public int getAmountLearnt(){
		return this.amountLearnt;
	}

}
